#include <iostream>
#include <cstdlib>
#include <conio.h>
#include <ctime>

using namespace std;
bool Agi = false;
char input[43]; // board have 42 place

int Playgame = 0;
int EVA = 0;

void BoardGame();
int Wingame();
int MinMax(int Depth);
int player();
int Value(int t);



void Clear() {
	Agi = false;
	for (int i = 0; i <= 80; i++)
		input[i] = ' ';
}

int Value(int column) { // store  in the array
	if (column > 7)
		return 0;
	int n;
	for (int i = 0; i <= 6; i++) {
		if (input[column + 7 * i] == ' ') {
			n = column + 7 * i;
			break;
		}
	}
	if (n > 42)
		return 0;
	return n;
}

int Wingame() { // Check for win case
	int temp = 0;
	for (int i = 1; i <= 42; i++) {
		if (input[i] != ' ')
		{
			temp++;
			if (i - int((i - 1) / 7) * 7 <= 4)
				if (input[i] == input[i + 1] && input[i] == input[i + 2] && input[i] == input[i + 3])
					if (input[i] == 'X')
						return 1;
					else
						return 2;
			if (i <= 21)
				if (input[i] == input[i + 7] && input[i] == input[i + 14] && input[i] == input[i + 21])
					if (input[i] == 'X')
						return 1;
					else
						return 2;
			if (i - int((i - 1) / 7) * 7 <= 4 && i <= 18)
				if (input[i] == input[i + 8] && input[i] == input[i + 16] && input[i] == input[i + 24])
					if (input[i] == 'X')
						return 1;
					else
						return 2;
			if (i - int((i - 1) / 7) * 7 >= 4 && i <= 21)
				if (input[i] == input[i + 6] && input[i] == input[i + 12] && input[i] == input[i + 18])
					if (input[i] == 'X')
						return 1;
					else
						return 2;
		}

	}
	if (temp == 42)
		return 3;
	return 0;
}
void BoardGame() { //Check for Draw case 
	cout << "X->player O->AI" << endl;
	cout << endl << "    1   " << "    2   " << "    3   " << "    4   " << "    5   " << "    6   " << "    7   " << endl;

	int j = 42;
	for (int i = 0; i <= 23; i++) {
		if (i % 4 == 0)
			cout << string(57, '-');
		else
		{
			if ((i - 2) % 4 == 0) {
				j = 42 - (0.25*i + 0.5) * 6 - ((0.25*i + 0.5) - 1);
				for (int i = 0; i <= 6; i++) {
					cout << "|" << "   " << input[j] << "   ";
					j++;
				}
				cout << "|";
			}
			else {
				for (int i = 0; i <= 6; i++)
					cout << "|" << string(7, ' ');
				cout << "|";
			}
		}
		cout << endl;
	}
	cout << string(57, '-');
}

void Player_Pos(char XO) { // Player  function
	int Store;
	cout << endl << "X_turn: ";
	while (true) {
		cin >> Store;
		Store = Value(Store);
		if (Store != 0) {
			input[Store] = XO;
			return;
		}
		else
			cout << "ERROR" << endl;
	}
}

int main() {
	srand(time(0));
	Clear();
	while (true) {
		input[player()] = 'O';
		system("cls");
		BoardGame();
		int winningtemp = Wingame();
		if (winningtemp != 0) {
			if (winningtemp == 1)
				cout << endl << "PLAYER WON !";
			else if (winningtemp == 2)
				cout << endl << "AI WON ! ";
			else if (winningtemp == 3)
				cout << "DRAW!!!";
			getch();
			Clear();
		}
		else
			Player_Pos('X');
	}
}
int player() { // AI 
	float chance[2] = { 9999999 , 0 };
	for (int column = 1; column <= 7; column++) {
		Playgame = 0;
		EVA = 0;
		int PlayNumber = Value(column);
		if (PlayNumber != 0) {

			input[PlayNumber] = 'O';
			if (Wingame() == 2) {
				input[PlayNumber] = ' ';
				return PlayNumber;
			}
			float temp = -(100 * MinMax(1));
			if (Playgame != 0)
				temp -= ((100 * EVA) / Playgame);
			if (-temp >= 100)
				Agi = true;
			if (chance[0] > temp) {
				chance[0] = temp;
				chance[1] = PlayNumber;
			}
			input[PlayNumber] = ' ';
		}
	}
	return chance[1];
}
int MinMax(int Depth) // MinMax 
{
	char XO;
	int PlayNumber[8] = { 0,0,0,0,0,0,0,0 };
	int chance = 0;
	if (Depth % 2 != 0)
		XO = 'X';
	else
		XO = 'O';
	for (int column = 1; column <= 7; column++)
		PlayNumber[column] = Value(column);
	for (int column = 1; column <= 7; column++) {
		if (PlayNumber[column] != 0) {
			input[PlayNumber[column]] = XO;
			if (Wingame() != 0) {
				Playgame++;
				if (XO == 'O')
					EVA++;
				else
					EVA--;
				input[PlayNumber[column]] = ' ';
				return -1;
			}
			input[PlayNumber[column]] = ' ';
		}
	}
	if (Depth <= 6) {

		for (int column = 1; column <= 7; column++) {
			int temp = 0;
			if (PlayNumber[column] != 0) {
				input[PlayNumber[column]] = XO;
				if (Wingame() != 0) {
					Playgame++;
					if (XO == 'O')
						EVA++;
					else
						EVA--;
					input[PlayNumber[column]] = ' ';
					return -1;
				}
				temp = MinMax(Depth + 1);
				if (column == 1)
					chance = temp;
				if (chance < temp)
					chance = temp;
				input[PlayNumber[column]] = ' ';
			}
		}
	}
	return -chance;

}